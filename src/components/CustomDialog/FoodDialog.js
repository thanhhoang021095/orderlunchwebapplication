import React, { useContext, useState } from "react";
// core components
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
// @material-ui/core components
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
// nodejs library to set properties for components
import PropTypes from "prop-types";
// call API function
import callApi from "utils/callApi/callApi"
// Theme Context
import { ThemeContext } from "ThemeContext";
// jss style
import styles from "assets/jss/material-dashboard-react/components/customDialogStyle.js";
import API_LINK from "utils/const/const";

const useStyles = makeStyles(styles);


export default function FoodDialog(props) {
  // Variables Declaration
  const classes = useStyles();
  const { openCustomDialog } = props;
  // NEW FOOD STATE
  const [newFoodName, setNewFoodName] = useState("");
  const [newFoodDescription, setNewFoodDescription] = useState("");
  const [newFoodPrice, setNewFoodPrice] = useState("");
  const [newFoodCategoryId, setNewFoodCategoryId] = useState();
  const [newFoodImageFile, setNewFoodImageFile] = useState({});
  // THEMECONTEXT STATE
  const { setExistFoodData, setFoodData, categoryData } = useContext(ThemeContext);
  // INIT FOOD DATA
  const initFoodData = async () => {
    const result = await callApi.get(`/Menu/${props.menuId}`);
    setFoodData(result);
    setExistFoodData(true);
  }
  // INPUT FOOD NAME
  const inputFoodName = (e) => {
    if (props.shouldEditFood) {
      props.setSingleFoodName(e.target.value);
    }
    else {
      setNewFoodName(e.target.value);
    }
  }
  // INPUT FOOD DESCRIPTION
  const inputFoodDescription = (e) => {
    if (props.shouldEditFood) {
      props.setSingleFoodDescription(e.target.value);
    }
    else {
      setNewFoodDescription(e.target.value);
    }
  }
  // INPUT FOOD PRICE
  const inputFoodPrice = (e) => {
    if (props.shouldEditFood) {
      props.setSingleFoodPrice(e.target.value);
    }
    else {
      setNewFoodPrice(e.target.value);
    }
  }

  // SELECT FOOD CATEGORY
  const selectCategory = (e) => {
    if (props.shouldEditFood) {
      props.setSingleFoodCategoryId(e.target.value);
    }
    else {
      setNewFoodCategoryId(e.target.value);
    }
  }
  // UPLOAD FOOD IMAGE
  const inputImage = (e) => {
    const image = e.target.files[0];
      setNewFoodImageFile(image);
  }
  // CREATE NEW FOOD DATA
  const createNewFood = async () => {
    // HANDLE WITH FORM DATA
    const formData = new FormData();
    formData.append("FileToUpload", newFoodImageFile);
    // PARSE IMAGE FILE TO OBJECT
    const parsedImageObj = await callApi.uploadImage("/Pic",
      formData
    );
    // HANDLE CREATE NEW FOOD
    try {
      const result = await callApi.post("/Foods", {
        name: newFoodName,
        description: newFoodDescription,
        price: newFoodPrice,
        menuId: props.menuId,
        categoryId: newFoodCategoryId,
        image: parsedImageObj.fileToUpload.fileName
      });
    }
    catch (error) {
      console.log(error)
    }
    props.handleCloseDialog()
    initFoodData()
  }
  // EDIT FOOD DATA
  const handleEditFood = async () => {
     // HANDLE WITH FORM DATA
     const formData = new FormData();
     formData.append("FileToUpload", newFoodImageFile);
     // PARSE IMAGE FILE TO OBJECT
     const parsedImageObj = await callApi.uploadImage("/Pic",
       formData
     );
    try {
      const result = await callApi.update(`/Foods/${props.menuId}`, {
        id: props.singleFoodId,
        name: props.singleFoodName,
        description: props.singleFoodDescription,
        price: props.singleFoodPrice,
        menuId: props.menuId,
        categoryId: props.singleFoodCategoryId,
        image: parsedImageObj.fileToUpload.fileName
      });
    }
    catch (error) { console.log(error) }
    props.handleCloseDialog();
    initFoodData()
  }

  // CLOSE DIALOG
  const closeDialog = (form) => {
    props.handleCloseDialog();
    props.setShouldEditFood(false);
  }

  return (
    <Dialog open={openCustomDialog} onClose={closeDialog} aria-labelledby="form-dialog-title"
      disableEscapeKeyDown={true} disableBackdropClick={true}
      BackdropProps={{
        classes: {
          root: classes.backdrop,
        }
      }}
      PaperProps={{
        classes:
          { elevation24: classes.dialogPaper }
      }}
    >
      <DialogTitle id="form-dialog-title" style={{
        textAlign: "center",
        paddingBottom: "5px"
      }} >
        {props.shouldEditFood ?
          <Typography className={classes.dialogTitle}>
            Update Food
          </Typography>
          :
          <Typography className={classes.dialogTitle}>
            Create New Food
        </Typography>
        }
      </DialogTitle>
      <DialogContent>
        {props.shouldEditFood ?
          <React.Fragment>
            <TextField
              onChange={inputFoodName}
              value={props.singleFoodName}
              autoFocus
              margin="dense"
              id="foodName"
              label="Food Name"
              type="foodName"
              fullWidth
            />
            <TextField
              onChange={inputFoodDescription}
              value={props.singleFoodDescription}
              autoFocus
              margin="dense"
              id="description"
              label="Description"
              type="description"
              fullWidth
            />
            <TextField
              onChange={inputFoodPrice}
              value={props.singleFoodPrice}
              autoFocus
              margin="dense"
              id="price"
              label="Price"
              type="price"
              fullWidth
            />
          </React.Fragment>
          :
          <React.Fragment>
            <TextField
              onChange={inputFoodName}
              autoFocus
              margin="dense"
              id="foodName"
              label="Food Name"
              type="foodName"
              fullWidth
            />
            <TextField
              onChange={inputFoodDescription}
              autoFocus
              margin="dense"
              id="description"
              label="Description"
              type="description"
              fullWidth
            />
            <TextField
              onChange={inputFoodPrice}
              autoFocus
              margin="dense"
              id="price"
              label="Price"
              type="price"
              fullWidth
            />
          </React.Fragment>
        }
        <div className={classes.selectAndUploadSection}>
          <FormControl className={classes.formControl} >
            <InputLabel htmlFor="grouped-select">Category</InputLabel>
            {props.shouldEditFood ?
              <Select value={props.singleFoodCategoryId} input={<Input id="grouped-select" />} onChange={selectCategory}>
                {categoryData.map(data => (
                  <MenuItem key={data.id} value={data.id}>
                    {data.name}
                  </MenuItem>
                ))}
              </Select>
              :
              <Select defaultValue="" input={<Input id="grouped-select" />} onChange={selectCategory}>
                {categoryData.map(data => (
                  <MenuItem key={data.id} value={data.id}>
                    {data.name}
                  </MenuItem>
                ))}
              </Select>
            }
          </FormControl>
          <div className={classes.uploadFoodContainer}>
            <Typography className={classes.uploadTitle}>Upload food image</Typography>
            {props.shouldEditFood ?
              <React.Fragment>
                <img src={`${API_LINK}/Pic?pictureName=${props.singleFoodImage}`} alt={props.singleFoodImage} style={{ height: "20x", width: "80%" }} />
                <input type="file" id="myFile" name="filename"
                  className={classes.inputFile} onChange={inputImage} />
              </React.Fragment>
              :
              <input type="file" id="myFile" name="filename"
                className={classes.inputFile} onChange={inputImage} />
            }
          </div>
        </div>
      </DialogContent>
      <DialogActions className={classes.functionBtnGroup}>
        <Button className={classes.cancelBtn} variant="contained" onClick={closeDialog} >
          Cancel
        </Button>
        {props.shouldEditFood ?
          <Button className={classes.createBtn} variant="contained" onClick={handleEditFood} >
            Update
          </Button>
          :
          <Button className={classes.createBtn} variant="contained" onClick={createNewFood} >
            Create
        </Button>
        }
      </DialogActions>
    </Dialog>
  )
};

FoodDialog.propTypes = {
  openCustomDialog: PropTypes.bool,
  // headerTitle: PropTypes.string,
  // successBtn: PropTypes.string,
};