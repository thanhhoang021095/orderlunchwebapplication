import React, { useContext, useState } from "react";
// core components
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
// @material-ui/core components
// nodejs library to set properties for components
import PropTypes from "prop-types";
// call API function
import callApi from "utils/callApi/callApi"
// Theme Context
import { ThemeContext } from "ThemeContext";
// jss style
import styles from "assets/jss/material-dashboard-react/components/customDialogStyle.js";

const useStyles = makeStyles(styles);

export default function SupplierDialog(props) {
  const classes = useStyles();
 
  // New Supplier Data
  const [newSupplierName, setNewSupplierName] = useState("");
  const [newSupplierAddress, setNewSupplierAddress] = useState("");
  const [newSupplierHotLine, setNewSupplierHotLine] = useState("");

  const { setSupplierData } = useContext(ThemeContext);
    
  // INIT SUPPLIER DATA
  const initSupplierData = async () => {
    const result = await callApi.get('/Suppliers')
    setSupplierData(result)
  };
  // INPUT SUPPLIER NAME
  const inputSupplierName = (e) => {
    if (props.shouldEditSupplier) {
      props.setSingleSupplierName(e.target.value)
      // singleSupplierData.name = singleSupplierName
    }
    else {
      setNewSupplierName(e.target.value)
    }
  };
  // INPUT SUPPLIER ADDRESS
  const inputSupplierAddress = (e) => {
    if (props.shouldEditSupplier) {
      props.setSingleSupplierAddress(e.target.value)
      // singleSupplierData.address = singleSupplierAddress
    }
    else {
      setNewSupplierAddress(e.target.value)
    }
  };
  // INPUT SUPPLIER HOTLINE
  const inputSupplierHotLine = (e) => {
    if (props.shouldEditSupplier) {
      props.setSingleSupplierHotLine(e.target.value)
      // singleSupplierData.hotLine = singleSupplierHotLine
    }
    else {
      setNewSupplierHotLine(e.target.value)
    }
  };
  // CREATE NEW SUPPLIER DATA
  const createNewSupplier = async () => {
    try {
      const result = await callApi.post('/Suppliers', {
        name: newSupplierName,
        address: newSupplierAddress,
        hotLine: newSupplierHotLine,
      })
      console.log(`You created product ${result.name} succesfully`);
    }
    catch (error) {
      console.log(error)
    }
    props.handleCloseDialog()
    initSupplierData()
  };
  // EDIT SUPPLIER DATA
  const handleEditSupplier = async () => {
    try {
      const result = await callApi.update(`/Suppliers/${props.singleSupplierId}`, {
        id: props.singleSupplierId,
        name: props.singleSupplierName,
        address: props.singleSupplierAddress,
        hotLine: props.singleSupplierHotLine,
      });
      console.log(`You updated product ${result.name} successfully`);
    }
    catch (error) { console.log(error) }
    props.handleCloseDialog();
    // setTimeout(function () {
    //   // window.alert("success")
    // }, 1000);
    // setOpenSnackBar(true)
    initSupplierData()
  };

  // CLOSE DIALOG
  const closeDialog = () => {
    props.handleCloseDialog()
    props.setShouldEditSupplier(false)
  };

  return (
    <Dialog open={props.openCustomDialog} onClose={closeDialog} aria-labelledby="form-dialog-title"
      disableEscapeKeyDown={true} disableBackdropClick={true}
      BackdropProps={{
        classes: {
          root: classes.backdrop,
        }
      }}
      PaperProps={{
        classes:
          { elevation24: classes.dialogPaper }
      }}
    >
      <DialogTitle id="form-dialog-title" style={{
        textAlign: "center",
        paddingBottom: "5px"
      }} >
        {props.shouldEditSupplier ?
          <Typography className={classes.dialogTitle}>
            Update Supplier
          </Typography>
          :
          <Typography className={classes.dialogTitle}>
            Create New Supplier
        </Typography>
        }
      </DialogTitle>
      <DialogContent>
        {props.shouldEditSupplier ?
          <>
            <TextField
              onChange={inputSupplierName}
              value={props.singleSupplierName}
              autoFocus
              margin="dense"
              id="name"
              label="Name"
              type="name"
              fullWidth
            />
            <TextField
              onChange={inputSupplierAddress}
              value={props.singleSupplierAddress}
              autoFocus
              margin="dense"
              id="address"
              label="Address"
              type="address"
              fullWidth
            />
            <TextField
              onChange={inputSupplierHotLine}
              value={props.singleSupplierHotLine}
              autoFocus
              margin="dense"
              id="hotLine"
              label="Hotline"
              type="hotLine"
              fullWidth
            />
          </>
          :
          <>
            <TextField
              onChange={inputSupplierName}
              autoFocus
              margin="dense"
              id="name"
              label="Name"
              type="name"
              fullWidth
            />
            <TextField
              onChange={inputSupplierAddress}
              autoFocus
              margin="dense"
              id="address"
              label="Address"
              type="address"
              fullWidth
            />
            <TextField
              onChange={inputSupplierHotLine}
              autoFocus
              margin="dense"
              id="hotLine"
              label="Hotline"
              type="hotLine"
              fullWidth
            />
          </>
        }
      </DialogContent>
      <DialogActions className={classes.functionBtnGroup}>
        <Button className={classes.cancelBtn} variant="contained" onClick={closeDialog} >
          Cancel
        </Button>
        {props.shouldEditSupplier ?
          <Button className={classes.createBtn} variant="contained" onClick={handleEditSupplier} >
            Update
          </Button>
          :
          <>
            <Button className={classes.createBtn} variant="contained" onClick={createNewSupplier} >
              Create
            </Button>
          </>
        }
      </DialogActions>
    </Dialog>
  )
};

SupplierDialog.propTypes = {
  openCustomDialog: PropTypes.bool,
  // headerTitle: PropTypes.string,
  // successBtn: PropTypes.string,
};