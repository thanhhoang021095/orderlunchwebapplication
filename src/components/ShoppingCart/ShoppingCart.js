import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import Badge from '@material-ui/core/Badge';
import Typography from '@material-ui/core/Typography';
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem"

// import parseData from "utils/commonService/commonService"
// import callApi from "utils/callApi/callApi"

import styles from "assets/jss/material-dashboard-react/components/shoppingCartStyle.js";
// import { ThemeContext } from 'ThemeContext';

const useStyles = makeStyles(styles);

export default function ShoppingCart() {
    const classes = useStyles();
    // const {cartData, setCartData} = useContext(ThemeContext)
    const [state, setState] = React.useState({
        right: false,
    });
    // useEffect(() => {
    //     const initData = async () => {
    //         const params = parseData({
    //           CartId: 2,
    //         })
    //         const result = await callApi.post('/FoodCarts' + params);
    //         setCartData([...cartData,...result]);
    //       }
    //       initData()
    //       console.log(cartData)
    // }, [])
    // ToggleDrawer
    const toggleDrawer = (side, open) => event => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setState({ ...state, [side]: open });
    };

    const sideList = side => (
        <div
            className={classes.drawerContainer}
            role="presentation"
            onClick={toggleDrawer(side, false)}
            onKeyDown={toggleDrawer(side, false)}
        >
            <List>
                {['Inbox', 'Starred', 'Send email', 'Drafts'].map((text, index) => (
                    <ListItem button key={text}>
                        <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
                        <ListItemText primary={text} />
                    </ListItem>
                ))}
            </List>
            <Divider style={{ margin: "5px 20px" }} />
            <div className={classes.purchaseContainer}>
                <GridContainer className={classes.totalPriceContainer}>
                    <GridItem sm={12} md={6} lg={6} className={classes.totalTitle}>
                        <Typography variant="h5" >
                            Total:
                        </Typography>
                    </GridItem>
                    <GridItem sm={12} md={6} lg={6} className={classes.priceTitle} >
                        <Typography variant="h5" >
                           125,000 VND
                        </Typography></GridItem>
                </GridContainer>
                <Button variant="contained" color="primary" className={classes.orderButton}>
                    Order Now
                </Button>
            </div>
        </div>
    );


    return (
        <div >
            <Button
                onClick={toggleDrawer('right', true)}
                className={classes.navLink} >
                <Badge badgeContent={1} color="primary" className={classes.productInCart}>
                    <ShoppingCartIcon className={classes.icons} />
                </Badge>
            </Button>
            <Drawer anchor="right" open={state.right} onClose={toggleDrawer('right', false)} style={{ width: "auto" }}>
                {sideList('right')}
            </Drawer>
        </div>
    );
}