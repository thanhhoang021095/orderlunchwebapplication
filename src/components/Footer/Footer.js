/*eslint-disable*/
import React from "react";
import {Link} from "react-router-dom"
// nodejs library to set properties for components
import PropTypes from "prop-types";
// nodejs library that concatenates classes
import classNames from "classnames";
// material-ui core components
import { List, ListItem } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

// components
import GridContainer from "components/Grid/GridContainer"
import GridItem from "components/Grid/GridItem"
// @material-ui/icons
import styles from "assets/jss/material-dashboard-react/components/footerStyle";

// import logo from "../../assets/img/fetch_logo.svg"

const useStyles = makeStyles(styles);

export default function Footer(props) {
  const classes = useStyles();
  const { whiteFont } = props;
  const footerClasses = classNames({
    [classes.footer]: true,
    [classes.footerWhiteFont]: whiteFont
  });

  return (
    <footer className={footerClasses}>
      <GridContainer className={classes.footerGrid} >
        <GridItem xs={12} sm={6} md={6} lg={6} className={classes.footerLeftGrid}>
          <div className={classes.left}>
            <List className={classes.list}>
              <ListItem className={classes.inlineBlock}>
                <Link to="/"
                  className={classes.block}
                  // style={{paddingLeft:"0px"}}
                >
                  <Button
                    className={classes.navLink}
                  >
                      Home
                  </Button>

                </Link>
              </ListItem>
              <ListItem className={classes.inlineBlock}>
                <a
                  href="https://fetch.tech/about-us"
                  className={classes.block}
                  target="_blank"
                >
                  <Button
                    className={classes.navLink}
                  >
                    About us
                  </Button>
                </a>
              </ListItem>
            </List>
          </div>
        </GridItem>
        <GridItem xs={12} sm={6} md={6} lg={6} className={classes.footerRightGrid}>
          <div className={classes.right}>
            &copy; {1900 + new Date().getYear()} made by{" "}
            <a
              className={classes.copyRightContent}
              target="_blank"
            >
              Fetch
            </a>
        </div>
        </GridItem>
      </GridContainer>
    </footer>
  );
}

Footer.propTypes = {
  whiteFont: PropTypes.bool
};
