function parseData (obj) {
    let result = '?';
    Object.keys(obj).forEach((key) => {
        result +=  key + '=' + obj[key] + '&'
    })
    result = result.substring(0, result.length - 1);
    return result;
}

export default parseData