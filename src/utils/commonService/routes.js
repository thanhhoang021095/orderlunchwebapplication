import SupplierSection from "views/AdminPage/Sections/SupplierSection"
import MenuSection from "views/AdminPage/Sections/MenuSection"
// import OrderSection from "views/AdminPage/Sections/OrderSection"


const routes = [
    // Approval Workflow
    {path: '/admin/supplier', exact: true, name: 'Supplier', component: SupplierSection},
    {path: '/admin/menu/:id', exact: true, name: 'Menu', component: MenuSection},
    // {path: '/admin/order', exact: true, name: 'Order', component: OrderSection},
    
]
export default routes