// const API = 'https://jsonplaceholder.typicode.com';
import API_LINK from '../const/const'

async function get(uri) {
    const result = await fetch(API_LINK + uri, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        },
        //  headers: {
        //      Authorization: 'Bearer ' + token

        // },
    });
    if (result.ok === true) {
        const data = await result.json();
        return data;
    }
}
async function post(uri, data, token) {
    // console.log(JSON.stringify(data))
    const result = await fetch(API_LINK + uri, {
        method: "POST",
        //  headers: {
        //      Authorization: 'Bearer ' + token

        // },
        headers: {
            Authorization: 'Bearer ' + token,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    if (result.ok === true) {
        // const data = await result.json();
          // return data;
          return result;
    }
    console.log("POST METHOD RESPONSE", result)
}

async function uploadImage(uri, data) {
    const result = await fetch(API_LINK + uri, {
        method: "POST",
        // headers: {
        //     'Content-Type': 'multipart/form-data'
        // },
        // responseType: 'arraybuffer',
        body: data
    })
    if (result.ok === true) {
        const data = await result.json();
        return data;
    }
    console.log("UPLOAD IMG RESPONSE", result)
};

async function login(uri, data, token) {
    // console.log(JSON.stringify(data))
    const result = await fetch(API_LINK + uri, {
        method: "POST",
        headers: {
            Authorization: 'Bearer ' + token,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    if (result.ok === true) {
        const data = await result.json();
        return data;
    }
};

async function register(uri, data) {
    // console.log(JSON.stringify(data))
    const result = await fetch(API_LINK + uri, {
        method: "POST",
        //  headers: {
        //      Authorization: 'Bearer ' + token

        // },
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    if (result.ok === true) {
        return true;
    }
    return false;
}


async function update(uri, data) {
    const result = await fetch(API_LINK + uri, {
        method: "PUT",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });
    if (result.ok ===true) {
        // const data = await result.json();
        return result;
    }
    console.log("UPDATE METHOD RESPONSE", result)
}

async function remove(uri, data) {
    const result = await fetch(API_LINK + uri, {
        method: "DELETE",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    console.log("DELETE METHOD RESPONSE", result)
}

export default {
    get,
    post,
    uploadImage,
    login,
    register,
    update,
    remove
};