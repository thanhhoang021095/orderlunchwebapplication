import React from "react";
import ReactDOM from 'react-dom';
import Main from "./Main"
import ThemeContextComponent from './ThemeContext';

ReactDOM.render(
  <ThemeContextComponent >
    <Main />
  </ThemeContextComponent>
  , document.getElementById("root")
);