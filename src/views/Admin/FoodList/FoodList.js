import React, { useContext, useEffect, useState } from "react";
// @material-ui/icons
import AddIcon from '@material-ui/icons/Add';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import GridItem from "components/Grid/GridItem.js";
import RegularButton from "components/CustomButtons/Button.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import FoodDialog from "components/CustomDialog/FoodDialog.js";
// theme context
import { ThemeContext } from "ThemeContext";
// call Api function
import callApi from "utils/callApi/callApi";
import API_LINK from 'utils/const/const'
// jss style
import styles from "assets/jss/material-dashboard-react/views/foodListStyle";
// local storage state

const useStyles = makeStyles(styles);

export default function FoodList(props) {
  const classes = useStyles();
  // COMPONENT STATE
  const [openDialog, setOpenDialog] = React.useState(false);
  const [shouldEditFood, setShouldEditFood] = useState(false)
  const [singleFoodData, setSingleFoodData] = useState({})
  const [singleFoodId, setSingleFoodId] = useState(0);
  const [singleFoodName, setSingleFoodName] = useState("");
  const [singleFoodDescription, setSingleFoodDescription] = useState("");
  const [singleFoodPrice, setSingleFoodPrice] = useState("");
  const [singleFoodCategoryId, setSingleFoodCategoryId] = useState(0);
  const [singleFoodImage, setSingleFoodImage] = useState("");
  // THEMECONTEXT STATE
  const { foodData, setFoodData, existFoodData,
    setExistFoodData, setCategoryData, dailyMenuData, setDailyMenuData } = useContext(ThemeContext)
  // URL ID VARIABLE
  const menuId = parseInt(props.match.params.id);
  // INIT FOOD DATA
  const initFoodData = async () => {
    const result = await callApi.get(`/Menu/${props.match.params.id}`)
    setFoodData(result);
    setExistFoodData(true);
  }
  // INIT CATEGORY DATA
  const initCategoryData = async () => {
    try {
      const result = await callApi.get("/Categories")
      setCategoryData(result)
    }
    catch (error) {
      console.log(error)
    }
  }
  useEffect(() => {
    initFoodData();
    initCategoryData();
  }, [])
  // ADD NEW FOOD
  const addNewFood = () => {
    setOpenDialog(true)
  }
  // OPEN EDIT FOOD MODAL
  const openEditModal = async function (id) {
    setOpenDialog(true);
    setShouldEditFood(true);
    setSingleFoodId(id);
    // INIT SINGLE FOOD DATA
    try {
      const result = await callApi.get(`/Foods/${id}`);
      // console.log("food data",result)
      setSingleFoodData(result);
      setSingleFoodName(result.name);
      setSingleFoodDescription(result.description);
      setSingleFoodPrice(result.price);
      setSingleFoodCategoryId(result.categoryId);
      setSingleFoodImage(result.image);
    }
    catch (error) {
      console.log(error)
    }
  }
  // DELETE Food
  const deleteFood = async function (id) {
    try {
      const result = await callApi.remove(`/Foods/${id}`, {
        id: id
      })
    }
    catch (error) {
      console.log(error)
    }
    initFoodData()
  }
  // CLOSE DIALOG
  const handleCloseDialog = () => {
    setOpenDialog(false);
  };
  // ADD FOOD TO DAILY MENU LIST
  const addFoodToDailyMenu = async function (id) {
    try {
      const result = await callApi.get(`/Foods/${id}`);
      setDailyMenuData([...dailyMenuData, result]);
    }
    catch (error) {
      console.log(error);
    };
  }

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12} lg={12}>
        <RegularButton color="primary" className={classes.addNewFoodBtn} onClick={addNewFood} >
          <AddIcon className={classes.addIcon} /> Add New Food
        </RegularButton >
        <FoodDialog openCustomDialog={openDialog}
          handleCloseDialog={handleCloseDialog}
        >
        </FoodDialog>
        <Card plain>
          <CardHeader plain color="primary">
            <h4 className={classes.cardTitleWhite}>
              Food List
            </h4>
          </CardHeader>
          <CardBody>
            {existFoodData &&
              <Table
                tableHeaderColor="primary"
                tableHead={["ID", "Name", "Description", "Price", "Image", ""]}
                tableData={
                  foodData && foodData.foods.map((data, key) =>
                    [
                      [<div key={key}>{data.id}</div>],
                      [<div key={key}>{data.name}</div>],
                      [<div key={key}>{data.description}</div>],
                      [<div key={key}>{data.price}</div>],
                      [<img key={key} src={`${API_LINK}/Pic?pictureName=${data.image}`} alt={data.image} style={{ height: "70px", width: "80%" }} />],
                      [
                        <div key={key} style={{ textAlign: "center", display: "inline-flex" }} >
                          <RegularButton
                            style={{ backgroundColor: "#2e7d32", fontSize: "12px", padding: "12px 18px", margin: "0px 8px" }}
                            onClick={() => openEditModal(data.id)}
                          >
                            Edit
                          </RegularButton>
                          <FoodDialog openCustomDialog={openDialog}
                            handleCloseDialog={handleCloseDialog}
                            // SINGLE FOOD PROPS
                            singleFoodData={singleFoodData}
                            setSingleFoodData={setSingleFoodData}
                            singleFoodId={singleFoodId}
                            singleFoodName={singleFoodName}
                            setSingleFoodName={setSingleFoodName}
                            singleFoodPrice={singleFoodPrice}
                            setSingleFoodPrice={setSingleFoodPrice}
                            singleFoodDescription={singleFoodDescription}
                            setSingleFoodDescription={setSingleFoodDescription}
                            singleFoodCategoryId={singleFoodCategoryId}
                            setSingleFoodCategoryId={setSingleFoodCategoryId}
                            singleFoodImage={singleFoodImage}
                            setSingleFoodImage={setSingleFoodImage}
                            shouldEditFood={shouldEditFood}
                            setShouldEditFood={setShouldEditFood}
                            // MENU ID 
                            menuId={menuId}
                          />
                          <RegularButton
                            style={{ backgroundColor: "#b71c1c", fontSize: "12px", padding: "12px 18px", margin: "0px 8px" }}
                            onClick={() => deleteFood(data.id)}
                          >
                            Delete
                          </RegularButton>
                          {/* <Link to={`/admin/menu/${data.id}`}> */}
                          <RegularButton
                            color="primary" style={{ fontSize: "12px", padding: "12px 18px", margin: "0px 8px" }}
                            onClick={() => addFoodToDailyMenu(data.id)}
                          >
                            Add To Daily Menu
                            </RegularButton>
                          {/* </Link> */}
                        </div>
                      ]
                    ])}
              />
            }
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  );
}
