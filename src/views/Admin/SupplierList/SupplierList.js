import React, { useEffect, useContext, useState } from "react";
// react-router-dom components
import { Link } from "react-router-dom"
// react plugin for creating charts
// @material-ui/core
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons
import AddIcon from '@material-ui/icons/Add';
// core components
import GridItem from "components/Grid/GridItem";
import GridContainer from "components/Grid/GridContainer";
import Table from "components/Table/Table";
import RegularButton from "components/CustomButtons/Button";
import Card from "components/Card/Card";
import CardHeader from "components/Card/CardHeader";
import CardBody from "components/Card/CardBody";
import SupplierDialog from 'components/CustomDialog/SupplierDialog';
// call api function
import callApi from "utils/callApi/callApi"
// jss style
import styles from "assets/jss/material-dashboard-react/views/dashboardStyle";
// theme context
import { ThemeContext } from "ThemeContext";

const useStyles = makeStyles(styles);

export default function SupplierList() {
  const classes = useStyles();
  // COMPONENT STATE
  const [openDialog, setOpenDialog] = useState(false);
  const [shouldEditSupplier, setShouldEditSupplier] = useState(false)
  const [singleSupplierData, setSingleSupplierData] = useState({})
  const [singleSupplierName, setSingleSupplierName] = useState("");
  const [singleSupplierAddress, setSingleSupplierAddress] = useState("");
  const [singleSupplierHotLine, setSingleSupplierHotLine] = useState("");
  const [singleSupplierId, setSingleSupplierId] = useState(0);

  // THEMECONTEXT STATE
  const { supplierData, setSupplierData} = useContext(ThemeContext);
  // INIT SUPPLIERDATA
  const initSupplierData = async () => {
    const result = await callApi.get('/Suppliers');
    setSupplierData(result);
  };
  useEffect(() => {
    initSupplierData();
  }, [])
  // ADD NEW SUPPLIER - CLICK CREATE NEW SUPPLIER BUTTON
  const addNewSupplier = () => {
    setOpenDialog(true);
  }
  // OPEN EDIT MODAL
  const openEditModal = async (id) => {
    setOpenDialog(true);
    setShouldEditSupplier(true);
    setSingleSupplierId(id);
    // INIT SINGLE SUPPLIER DATA
    try {
      const result = await callApi.get(`/Suppliers/${id}`);
      setSingleSupplierData(result);
      setSingleSupplierName(result.name);
      setSingleSupplierAddress(result.address);
      setSingleSupplierHotLine(result.hotLine);
    }
    catch (error) {
      console.log(error)
    };
  };
  // DELETE SUPPLIER
  const deleteSupplier = async function (id) {
    try {
      const result = await callApi.remove(`/Suppliers/${id}`, {
        id: id
      })
    }
    catch (error) {
      console.log(error);
    }
    initSupplierData()
  }
  // CLOSE MODAL
  const handleCloseDialog = () => {
    setOpenDialog(false);
  };
  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={12} md={12} lg={12}>
          <RegularButton color="primary" className={classes.addNewSupplierBtn} onClick={addNewSupplier}>
            <AddIcon className={classes.addIcon} /> Add New Supplier
          </RegularButton>
          <SupplierDialog
            openCustomDialog={openDialog}
            handleCloseDialog={handleCloseDialog}
          >
          </SupplierDialog>
          <Card>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>Supplier List</h4>
            </CardHeader>
            <CardBody>
              <Table
                tableHeaderColor="primary"
                tableHead={["ID", "Name", "Address", "Hotline", ""]}
                tableData={
                  supplierData.map((data,key) =>
                    [
                      [<div key={key}>{data.id}</div>],
                      [<div key={key}>{data.name}</div>],
                      [<div key={key}>{data.address}</div>],
                      [<div key={key}>{data.hotLine}</div>],
                      [
                        <div key={key} style={{ textAlign: "center" }}>
                          <RegularButton
                            style={{ backgroundColor: "#2e7d32", fontSize: "12px", padding: "12px 18px", margin: "0px 15px" }}
                            onClick={() => openEditModal(data.id)}
                          >
                            Edit
                          </RegularButton>
                          <SupplierDialog openCustomDialog={openDialog}
                            handleCloseDialog={handleCloseDialog}
                            // SINGLE SUPPLIER PROPS
                            singleSupplierData={singleSupplierData}
                            setSingleSupplierData={setSingleSupplierData}
                            singleSupplierId={singleSupplierId}
                            singleSupplierAddress={singleSupplierAddress}
                            setSingleSupplierAddress={setSingleSupplierAddress}
                            singleSupplierName={singleSupplierName}
                            setSingleSupplierName={setSingleSupplierName}
                            singleSupplierHotLine={singleSupplierHotLine}
                            setSingleSupplierHotLine={setSingleSupplierHotLine}
                            shouldEditSupplier={shouldEditSupplier}
                            setShouldEditSupplier={setShouldEditSupplier}
                          >
                          </SupplierDialog>
                          <RegularButton
                            style={{ backgroundColor: "#b71c1c", fontSize: "12px", padding: "12px 18px", margin: "0px 15px" }}
                            onClick={() => deleteSupplier(data.id)}
                          >
                            Delete
                          </RegularButton>
                          <Link to={`/admin/foods/${data.id}`}>
                            <RegularButton
                              color="primary" style={{ fontSize: "12px", padding: "12px 18px", margin: "0px 15px" }}
                            >
                              View Menu
                          </RegularButton>
                          </Link>
                        </div>
                      ],
                    ]
                  )
                }
              />
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    </div>
  );
}
