
import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
// core components
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import Card from "components/Card/Card";
// @material-ui/core components
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
// jss style
import styles from "assets/jss/material-dashboard-react/views/adminDailyMenuStyle.js";
// call api
import callApi from "utils/callApi/callApi";
import API_LINK from "utils/const/const";
const useStyles = makeStyles(styles);

export default function AdminDailyMenu() {
    const classes = useStyles();
    const [dailyMenuList, setDailyMenuList] = useState([])
    useEffect(() => {
        const initFoodListData = async () => {
            const result = await callApi.get("/DailyMenu/Today")
            setDailyMenuList(result.foods)
        }
        initFoodListData()
    }, [])
    return (
        <div>
            <GridContainer>
                <Typography  >
                   Daily Menu
                </Typography>
                {dailyMenuList && dailyMenuList.map((key, data) =>
                    <GridItem lg={4} md={6} sm={12} key={key}>
                        <Card className={classes.card}>
                            <CardActionArea >
                                <img src={`${API_LINK}/Pic?pictureName=${data.image}`} alt={data.image} style={{ height: "100px" }} />
                            </CardActionArea>
                            <CardContent className={classes.proCardContent}>
                                <GridContainer className={classes.proCardContainer} >
                                    {/* <GridItem xs={6} sm={6} md={6} lg={6} className={classes.proPriceGrid}> */}
                                    <Typography className={classes.proPrice} >
                                        {data.name}
                                    </Typography>
                                    <Typography className={classes.proPrice} >
                                        {data.description}
                                    </Typography>
                                    <Typography className={classes.proPrice} >
                                        {data.price}
                                    </Typography>
                                    <Typography className={classes.proPrice} >
                                        {data.menuId}
                                    </Typography>
                                    {/* </GridItem>
                                    <GridItem xs={6} sm={6} md={6} lg={6} className={classes.addToCartGrid}>
                                        Button
                                    </GridItem> */}
                                </GridContainer>
                            </CardContent>
                        </Card>
                    </GridItem>
                )}
            </GridContainer>
        </div>
    )
}