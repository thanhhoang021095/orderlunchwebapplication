import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// import Divider from '@material-ui/core/Divider';
// import Typography from '@material-ui/core/Typography';

// @material-ui/icons
// core components
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
// import Card from "components/Card/Card"
// import CardActionArea from '@material-ui/core/CardActionArea';

// import InfoArea from "components/InfoArea/InfoArea";
// import SelectMenu from "components/SelectMenu/SelectMenu";
// import Category from "components/Category/Category"

// import parseData from "utils/commonService/commonService"
// import callApi from "utils/callApi/callApi"


import styles from "assets/jss/material-dashboard-react/views/foodPageStyle.js";
// import callApi from "utils/callApi/callApi";

const useStyles = makeStyles(styles);

export default function FoodList() {
  const classes = useStyles();
  // const { foodData, setFoodData } = useContext(ThemeContext)

  // const initDailyMenuData = async () => {
  //   // const params = parseData({
  //   //   MenuId: 2,
  //   //   FoodCategoryId: 1,
  //   //   PageSize: 12,
  //   //   CurrentPage: 1
  //   // })
  //   const result = await callApi.get('/DailyMenu/1');
  //   setFoodData(result);
  // }
  // useEffect(() => {
  //   initDailyMenuData()
  //   console.log("Daily Menu Data", foodData)
  // }, [])
  // // Pass Single Supplier Data To Dialog
  // const passDataToDialog = async function (id) {
  //   setOpenDialog(true)
  //   setShouldUpdateSupplier(true);
  //   console.log("supplier is update?", shouldUpdateSupplier)
  //   try {
  //     const result = await callApi.get(`/Suppliers/${id}`)
  //     console.log("get Single Supplier Data to Update", result)
  //     setSingleSupplierData(result)
  //     setSingleSupplierName(result.name)
  //     setSingleSupplierAddress(result.address)
  //     setSingleSupplierHotLine(result.hotLine)
  //   }
  //   catch (error) {
  //     console.log(error)
  //   }
  // }
  // // Delete Single Supplier Function
  // const deleteFood = async function (id) {
  //   try {
  //     const result = await callApi.remove("/Foods/Id", {
  //       id: id
  //     })
  //     console.log("Delete API Response", result)
  //   }
  //   catch (error) {
  //     console.log(error)
  //   }
  //   initSupplierData()
  // }

  return (
    <div className={classes.section}>
      <GridContainer className={classes.proMainContainer} >
        {/* <GridItem xs={12} sm={6} md={4} lg={3} className={classes.categoryGrid}>
          <Category />
        </GridItem> */}
        <GridItem xs={12} sm={6} md={8} lg={9} className={classes.proSectionGrid}>
          <GridContainer className={classes.proContainer} >
            {/* {dailyMenuData.map((elm, key) => (
              <GridItem key={key} xs={12} sm={12} md={6} lg={4} className={classes.proGrid}>
                <Card>
                  <CardActionArea >
                    <div
                      style={{ background: `url(${elm.image}) center center/cover no-repeat` }}
                      className={classes.media} alt="" >
                    </div>
                  </CardActionArea>
                </Card>
              </GridItem>
            ))} */}
          </GridContainer>
        </GridItem>
      </GridContainer>

    </div>
  );
}
