import React, { useState } from 'react';


export const ThemeContext = React.createContext("");
function ThemeContextComponent({ children }) {

    // User Data
   
    const [isLoged, setIsLoged] = useState(false);
    const [cartData, setCartData] = useState([]);
    const [isAdmin, setIsAdmin] = useState(false);

    // Supplier Data
    const [supplierData, setSupplierData] = useState([]);
   
    // Menu Data
    const [dailyMenuData, setDailyMenuData] = useState([]);
    // Category Data
    const [categoryData, setCategoryData] = useState([]);

    // Food Data
    const [ foodData , setFoodData] = useState([]);
    const [existFoodData, setExistFoodData] = useState(false);
    const [ foodListId, setFoodListId] = useState(0);
  
    return (
        <ThemeContext.Provider value={{
            cartData, setCartData,
            isAdmin, setIsAdmin,
            isLoged, setIsLoged,
            supplierData, setSupplierData,
            dailyMenuData, setDailyMenuData,
            existFoodData, setExistFoodData,
            foodData , setFoodData,
            categoryData, setCategoryData,
            foodListId, setFoodListId
            
        }}>
            {children}
        </ThemeContext.Provider>
    )
}

export default ThemeContextComponent;