const adminDailyMenuStyle = {
    infoArea: {
        maxWidth: "360px",
        margin: "0 auto",
        padding: "0px",
    },
    card: {
        maxWidth: 345,
        boxShadow: "none",
        borderRadius: "0px"
    },
    media: {
        height: 140,
        width: "100%",
    },
    icon: {
        width: "36px",
        height: "36px"
    },
    descriptionWrapper: {
        overflow: "hidden"
    },
    description: {
        overflow: "hidden",
        marginTop: "0px",
        fontSize: "14px"
    },
    iconWrapperVertical: {
        float: "none"
    },
    iconVertical: {
        width: "61px",
        height: "61px"
    },

    proName: {
        width: "240px",
        whiteSpace: "nowrap",
        marginBottom: "12px",
        padding: "0px",
        overflow: "hidden !important",
        textOverflow: "ellipsis",
        color: "#25282b",
        fontSize: "21px",
        fontWeight: 500,
        textAlign: "start",
        fontFamily: '-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif',
        "&:hover": {
            color: "#ff5722",
            cursor: "pointer"
        },
    },
    proPrice: {
        fontSize: "21px",
        fontWeight: 700,
        color: "#000",
        marginTop: "4px",
        fontFamily: '-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif'
    },
    proCardContent: {
        border: "1px solid #eeeeee",
        padding: "15px !important",
        backgroundColor: "rgba(252, 251, 249, 0.68)"
    },
    proCardContainer: {
        padding: "0px"
    },
    proPriceGrid: {
        textAlign: "left",
        whiteSpace: "nowrap"
    },
    addToCartGrid: {
        textAlign: "right",
        paddingRight: "15px"
    },
    '@media (max-width:350px)': {
        proPrice: {
            fontSize: "16px"
        }
    },

}
export default adminDailyMenuStyle;