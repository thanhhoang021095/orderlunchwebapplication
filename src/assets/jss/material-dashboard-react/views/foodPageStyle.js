import { title } from "assets/jss/material-dashboard-react";

const foodPageStyle = {
  section: {
    margin: "0 20px",
    textAlign: "center",
    paddingBottom: "20px"
  },
  title: {
    ...title,
    marginBottom: "1rem",
    marginTop: "30px",
    minHeight: "32px",
    textDecoration: "none"
  },
  description: {
    color: "#999"
  },
  proHeader: {
    padding: "20px 15px",
    marginLeft: "0px",
    marginRight: "0px",
  },
  proMainContainer: {
    margin: "0px",
    paddingTop: "20px"
  },
  divider: {
    width: "100%",
    padding: "0px 15px"
  },
  searchingResults: {
    color: "#333366",
    fontSize: "16px",
    letterSpacing: "1px",
    fontFamily: ' -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif'
  },
  resultContent: {
    display: "inline",
    color: "#ff5722 !important",
    fontWeight: 700,
    marginRight: "8px"
  },
  leftHeaderGrid: {
    textAlign: "left",
    margin: "auto 0px",
    paddingLeft: "0px"
  },
  rightHeaderGrid: {
    textAlign: "right",
    paddingRight: "0px"
  },
  categoryGrid: {
    paddingLeft: "15px !important",
    // paddingRight:"24px !important"
  },
  proSectionGrid: {
    padding: "0px",
  },
  proContainer: {
    marginLeft: "0px",
    marginRight: "0px",
  },
  proGrid: {
    paddingBottom: "30px",
  },
  '@media (max-width:450px)': {
    searchingResults: {
      fontSize: "10px"
    }
  },
};

export default foodPageStyle;
