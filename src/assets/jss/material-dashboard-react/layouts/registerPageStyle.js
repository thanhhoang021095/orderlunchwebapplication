import { jssContainer } from "assets/jss/material-dashboard-react";

const registerPageStyle = {
  container: {
    ...jssContainer,
    zIndex: "2",
    position: "relative",
    color: "#FFFFFF",
    height:"100vh !important"
    // padding: "130px 20px 85px 20px",
  },
  cardHidden: {
    opacity: "0",
    transform: "translate3d(0, -60px, 0)"
  },
  pageHeader: {
    height: "100vh !important",
    display: "inherit",
    position: "relative",
    margin: "0",
    padding: "0",
    border: "0",
    alignItems: "center",
    "&:before": {
      background: "rgba(0, 0, 0, 0.5)"
    },
    "&:before,&:after": {
      position: "absolute",
      zIndex: "1",
      width: "100%",
      display: "block",
      left: "0",
      top: "0",
      content: '""'
    },
    "& footer li a,& footer li a:hover,& footer li a:active": {
      color: "#FFFFFF"
    },
    "& footer": {
      position: "absolute",
      bottom: "0",
      width: "100%"
    }
  },

  registerFormItem:{
    // margin:"auto 0px",
    // paddingBottom:"20px",
  },
  form: {
    margin: "0"
  },
  cardHeader: {
    width: "auto",
    textAlign: "center",
    marginLeft: "20px",
    marginRight: "20px",
    marginTop: "-40px",
    padding: "8px 0",
    marginBottom: "15px",
    backgroundColor: "#ff5722 !important",
    color: "#ffffff"
  },
  registerTitle: {
    fontFamily: '-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif',
    fontSize: "25px",
    fontWeight: 300,
    letterSpacing:"3px",
    margin:"20px"
  },
  inputSection: {
    padding: "0px 20px"
  },
  registerBtn:{
    color:"#000000",
    padding:"8px",
    letterSpacing:"1px",
    width:"100%",
    backgroundColor:"#f4ff81",
    boxShadow:"none",
    fontWeight:400,
    fontSize:"16px",
    fontFamily:'-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif',
    "&:hover":{
      width:"100%",
      backgroundColor:"#c2185b",
      color:"#ffffff"
    },
  },
  divider: {
    marginTop: "30px",
    marginBottom: "0px",
    textAlign: "center"
  },
  cardFooter: {
    padding: "0px 20px",
    border: "0",
    borderRadius: "6px",
    justifyContent: "center !important",
    margin:"0px !important"
  },
  registerForm: {
    // marginBottom:"0px !important"
  },
  chooseRoleForm:{
    marginTop:"12px",
    padding:"3px 20px",
    width:"100%"
  },
  loginSection:{
    padding: "20px",
    paddingTop:"10px",
    border: "0",
    justifyContent: "center"
  },
  loginContainer:{
    margin:"0px"
  },
  loginTitleGrid:{
    textAlign:"center"
  },
  loginBtnGrid:{
    textAlign:"center",
    padding:"0px",
  },
  loginLink:{
    marginLeft:"10px",
    color:"#ff5722",
    fontSize:"16px",
    textDecoration: "underline"
  }
};
export default registerPageStyle;
