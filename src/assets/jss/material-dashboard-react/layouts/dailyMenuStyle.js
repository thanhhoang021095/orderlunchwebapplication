import { jssContainer, title } from "assets/jss/material-dashboard-react";

const dailyMenuStyle = {
  container: {
    zIndex: "12",
    color: "#FFFFFF",
    ...jssContainer
  },
  title: {
    ...title,
    display: "inline-block",
    position: "relative",
    marginTop: "30px",
    minHeight: "32px",
    color: "#FFFFFF",
    textDecoration: "none"
  },
  subtitle: {
    fontSize: "1.313rem",
    maxWidth: "500px",
    margin: "10px auto 0"
  },
  main: {
    background: "#FFFFFF",
    position: "relative",
    zIndex: "3",
    
  },
  mainRaised: {
    borderRadius: "0px",
    marginTop:"80px",
    minHeight:"435px"
  }
};

export default dailyMenuStyle;
