import { jssContainer } from "assets/jss/material-dashboard-react";

const loginPageStyle = {
  container: {
    ...jssContainer,
    zIndex: "2",
    position: "relative",
    color: "#FFFFFF",
  },
  cardHidden: {
    opacity: "0",
    transform: "translate3d(0, -60px, 0)"
  },
  pageHeader: {
    height: "100vh",
    display: "inherit",
    position: "relative",
    margin: "0",
    padding: "0",
    border: "0",
    alignItems: "center",
    "&:before": {
      background: "rgba(0, 0, 0, 0.5)"
    },
    "&:before,&:after": {
      position: "absolute",
      zIndex: "1",
      width: "100%",
      display: "block",
      left: "0",
      top: "0",
      content: '""'
    },
    "& footer li a,& footer li a:hover,& footer li a:active": {
      color: "#FFFFFF"
    },
    "& footer": {
      position: "absolute",
      bottom: "0",
      width: "100%"
    }
  },
  loginFormItem:{
    margin:"auto 0px"
  },
  form: {
    margin: "0"
  },
  cardHeader: {
    width: "auto",
    textAlign: "center",
    marginLeft: "20px",
    marginRight: "20px",
    marginTop: "-40px",
    padding: "10px 0",
    marginBottom: "15px",
    backgroundColor:"#ff5722 !important",
    color:"#ffffff"
  },
  loginTitle:{
    fontFamily:'-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif',
    fontSize: "25px",
    fontWeight:300,
    letterSpacing:"3px",
    margin:"20px"
  },
  divider: {
    marginTop: "30px",
    marginBottom: "0px",
    textAlign: "center"
  },
  cardFooter: {
    padding: "10px 20px",
    border: "0",
    borderRadius: "6px",
    justifyContent: "center !important"
  },
  registerSection:{
    padding: "20px",
    paddingTop:"10px",
    border: "0",
    justifyContent: "center"
  },
  registerContainer:{
    margin:"0px"
  },
  registerTitleGrid:{
    textAlign:"center"
  },
  registerBtnGrid:{
    textAlign:"center",
    padding:"0px",
  },
  inputIconsColor: {
    color: "#495057"
  },
  loginBtn:{
    color:"#212121",
    padding:"8px",
    letterSpacing:"1px",
    boxShadow:"none",
    width:"100%",
    backgroundColor:"#f4ff81",
    fontSize:"16px",
    fontWeight:400,
    fontFamily:'-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif',
    "&:hover":{
      width:"100%",
      backgroundColor:"#c2185b",
      color:"#ffffff"
    },
  },
  loginForm:{
    height:"100vh"
    // marginBottom:"0px !important"
  },
  inputSection:{
    padding:"20px"
  },
  registerBtn:{
    letterSpacing:"1px",
    fontFamily:'-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif',
    width:"100%",
    backgroundColor:"#ffffff",
    color:"#c2185b",
    boxShadow:"none",
    padding:"8px",
    fontSize:"16px",
    fontWeight:400,
    "&:hover":{
      color:"#ffffff",
      padding:"8px",
      fontSize:"16px",
      backgroundColor:"#c2185b"
    }
  },
  // '@media (max-width:840px)':{
  //   loginFormItem:{
  //     paddingBottom:"50px"
  //   }
  // },
  // '@media (max-width:480px)':{
  //   loginFormItem:{
  //     paddingBottom:"135px"
  //   }
  // },
};

export default loginPageStyle;
