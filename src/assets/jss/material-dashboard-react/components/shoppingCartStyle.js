const shoppingCartStyle = theme => ({
  drawerContainer: {
    width: 400,
  },
  navLink: {
    color: "inherit",
    position: "relative",
    padding: "0.9rem 20px",
    fontWeight: "400",
    fontSize: "12px",
    textTransform: "uppercase",
    borderRadius: "3px",
    lineHeight: "20px",
    textDecoration: "none",
    margin: "0px",
    display: "inline-flex",
    "&:hover,&:focus": {
      color: "inherit",
      textAlign: "center",
      background: "rgba(200, 200, 200, 0.2)"
    },
    [theme.breakpoints.down("sm")]: {
      textAlign: "center",
    }
  },
  productInCart: {
    padding: '0 4px',
  },
  icons: {
    fontSize: "21px"
  },
  purchaseContainer: {
    textAlign: "center"
  },
  totalPriceContainer: {
    margin: "16px"
  },
  totalTitle: {
    textAlign: "left",
    paddingLeft: "0px",
    paddingRight:"0px"
  },
  priceTitle: {
    textAlign: "right",
    paddingRight: "0px",
    paddingLeft: "0px",
  },
  orderButton: {
    margin: "10px auto",
    textTransform: "capitalize",
    fontFamily: '"Source Sans Pro", sans-serif',
    fontWeight: 700,
    width: "92%",
    height: "50px",
    fontSize: "22px",
    borderRadius: "0px",
    boxShadow: "none",
    backgroundColor: "#2e7d32",
    letterSpacing: "2px",
    "&:hover": {
      backgroundColor: "#e91e63"
    },
  },
  '@media (max-width: 960px)': {
    totalTitle :{
      textAlign:"center"
    },
    priceTitle:{
      textAlign:"center"
    }
  },
  '@media (max-width: 500px)': {
    drawerContainer :{
      width:"250px"
    }
  }
})

export default shoppingCartStyle