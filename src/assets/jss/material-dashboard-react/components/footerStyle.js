import { primaryColor } from "assets/jss/material-dashboard-react.js";
import header_cover from "assets/img/header-cover.png"

const footerStyle = theme => ({
  block: {
    padding:"15px 0px",
    color: "inherit",
    fontWeight: "500",
    fontSize: "16px",
    textTransform: "capitalized",
    borderRadius: "3px",
    textDecoration: "none",
    position: "relative",
    cursor:"pointer",
    display: "block",
    fontFamily:'-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif',
    "&:hover":{
      color:"inherit"
    },
  },
  left: {
    // float: "left! important",
    display: "block"
  },
  right: {
    padding: "15px 0px 15px 20px",
    margin: "0",
    // float: "right!important",
    fontSize:"16px",
    fontWeight:400,
    fontFamily:'-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif',
  },
  footer: {
    padding: "0.9375rem 35px",
    textAlign: "center",
    display: "flex",
    zIndex: "10",
    height:"80px",
    position: "relative",
    background:`url(${header_cover}) center center/cover no-repeat`,
    color:"#ffffff"
  },
  a: {
    color: primaryColor,
    textDecoration: "none",
    backgroundColor: "transparent",
  },
  footerWhiteFont: {
    "&,&:hover,&:focus": {
      color: "#FFFFFF"
    }
  },
  
  list: {
    marginBottom: "0",
    padding: "0",
    marginTop: "0"
  },
  inlineBlock: {
    display: "inline-block",
    padding: "0px",
    width: "auto",
  },
  icon: {
    width: "18px",
    height: "18px",
    position: "relative",
    top: "3px"
  },
  copyRightContent: {
    fontWeight: "500",
    fontSize: "16px",
    textTransform: "capitalized",
    color:"inherit",
    fontFamily:'-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif',
    "&:hover":{
      color:"inherit"
    },
  },
  footerGrid:{
    width:"100%",
    margin:"0px"
  },
  footerLeftGrid:{
    padding:"0px",
    textAlign:"left !important",
  },
  footerRightGrid:{
    padding:"0px",
    textAlign:"right",
    paddingRight:"0px",
    margin:"auto 0px"
  },
  logo:{
    height:"50px",
    marginLeft:"20px",
    marginRight:"20px"
  },
  navLink :{
    color: "inherit",
    position: "relative",
    padding: "15px 20px",
    fontWeight: 600,
    fontSize: "14px",
    textTransform: "capitalize",
    borderRadius: "3px",
    height: "50px",
    textDecoration: "none",
    margin: "0px",
    display: "inline-flex",
    "&:hover,&:focus": {
      color: "inherit",
      textAlign: "center",
      background: "rgba(200, 200, 200, 0.2)"
    },
    [theme.breakpoints.down("sm")]: {
      textAlign: "center",
    }
  },
  link :{
    fontWeight: 600,
    fontSize: "14px",
    color:"#ffffff",
    "&:hover":{
      color:"#ffffff"
    }
  },
  '@media (max-width: 600px)': {
    footerLeftGrid: {
      textAlign:"center"
    },
    footerRightGrid:{
      textAlign:"center"
    }
  }
});
export default footerStyle;
