const customDialogStyle = {
    container: {
        // marginLeft:"380px"
        // margin:"auto 380px"
    },
    formTitle: {
        textAlign: "center",
        color: "#ff5722",
        fontWeight: 500,
        fontSize: "27px",
    },
    dialogTitle: {
        fontSize: "25px"
    },
    card: {
        margin: "0px",
        boxShadow: "none"
    },
    addSupllierBtn: {
        width: "100%",
        fontSize: "14px",
        boxShadow: "none",
        backgroundColor: "#2e7d32",
        color: "#fff",
        padding: "8px"
    },

    inputSection: {
        paddingTop: "0px !important"
    },
    functionBtnGroup: {
        justifyContent: "center"
    },
    formControl: {
        width: "100%"
    },
    uploadTitle: {
        margin: "20px 0px 10px 0px"
    },
    cancelBtn: {
        boxShadow: "none",
        fontSize: "14px",
        padding: "6px 15px",
        margin: "0px 10px",
        color: "#fff",
        backgroundColor: "#f44336",
        border: "2px solid #f44336",
        marginLeft: "12px",
        "&:hover": {
            color: "#fff",
            backgroundColor: "#f44336",
            border: "2px solid #f44336",
        }
    },
    createBtn: {
        boxShadow: "none",
        fontSize: "14px",
        padding: "6px 15px",
        margin: "0px 10px",
        backgroundColor: "#388e3c",
        color: "#fff",
        border: "2px solid #388e3c",
        "&:hover": {
            backgroundColor: "#388e3c",
            color: "#fff",
            border: "2px solid #388e3c",
        }
    },
    backdrop: {
        backgroundColor: "rgba(0,0,0,0.1)"
    },
    dialogPaper: {
        maxWidth: "400px !important",
        boxShadow: "none"
    },
    inputFile: {
        

    }

}
export default customDialogStyle