import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import HeaderLinks from "components/Header/HeaderLinks.js";

import styles from "assets/jss/material-dashboard-react/layouts/dailyMenuStyle.js";

// Sections for this page
import UserDailyMenu from "views/User/UserDailyMenu/UserDailyMenu";

const dashboardRoutes = [];

const useStyles = makeStyles(styles);

export default function DailyMenu(props) {
    const classes = useStyles();
    const { ...rest } = props;
    return (
        <div>
            <Header
                color="transparent"
                routes={dashboardRoutes}
                rightLinks={<HeaderLinks />}
                fixed
                changeColorOnScroll={{
                    height: 400,
                    color: "white"
                }}
                {...rest}
            />
            <div className={classNames(classes.main, classes.mainRaised)}>
                <UserDailyMenu />
            </div>
            <Footer />
        </div >
    )
}