import React, {  useState } from "react";
import { Link } from "react-router-dom";

// @material-ui/core components
import { Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
// @material-ui/icons
import LockIcon from '@material-ui/icons/Lock';
import MailIcon from '@material-ui/icons/Mail';
import PersonIcon from '@material-ui/icons/Person';
// core components
// import Header from "components/Header/Header.js";
// import HeaderLinks from "components/Header/HeaderLinks.js";
// import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import CardFooter from "components/Card/CardFooter.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import callApi from "utils/callApi/callApi"

import styles from "assets/jss/material-dashboard-react/layouts/registerPageStyle";

import image from "assets/img/food-bg.jpg";

const useStyles = makeStyles(styles);

export default function RegisterPage(props) {
    const [cardAnimaton, setCardAnimation] = React.useState("cardHidden");
    // COMPONENT STATE
    const [userName, setUserName] = useState("");
    const [passWord, setPassWord] = useState("");
    const [email, setEmail] = useState("")
    setTimeout(function () {
        setCardAnimation("");
    }, 700);
    const classes = useStyles();
    // INPUT USERNAME
    const inputUsername = (e) => {
        setUserName(e.target.value);
    }
    // INPUT PASSWORD
    const inputPassword = (e) => {
        setPassWord(e.target.value);
    }
    // INPUT EMAIL
    const inputEmail = (e) => {
        setEmail(e.target.value)
    }
    // HANDLE REGISTER
    const handleRegister = async () => {
        const result = await callApi.register('/Account/Register', {
            username: userName,
            email: email,
            password: passWord,
        });
        if (result === true) {
            alert("REGISTER SUCCESS");
            props.history.push("/login");
        }
        alert("REGISTER FAILED");
    }
    return (
        <div>
            <div
                className={classes.pageHeader}
                style={{
                    backgroundImage: "url(" + image + ")",
                    backgroundSize: "cover",
                    backgroundPosition: "top center"
                }}
            >
                <div className={classes.container}>
                    <GridContainer justify="center" style={{ height: "100vh", padding: "30px 0px" }} className={classes.registerForm}>
                        <GridItem lg={12} sm={12} md={12} className={classes.registerFormItem}>
                            <Card className={classes[cardAnimaton]}>
                                <form className={classes.form}>
                                    <CardHeader className={classes.cardHeader}>
                                        <h4 className={classes.registerTitle}>Register Form</h4>

                                    </CardHeader>
                                    <CardBody className={classes.inputSection}>
                                        <CustomInput
                                            labelText="Username..."
                                            id="username"
                                            formControlProps={{
                                                fullWidth: true
                                            }}
                                            inputProps={{
                                                onChange: inputUsername,
                                                type: "username",
                                                endAdornment: (
                                                    <InputAdornment position="end">
                                                        <PersonIcon className={classes.inputIconsColor} />
                                                    </InputAdornment>
                                                )
                                            }}
                                        />
                                        <CustomInput
                                            labelText="Email"
                                            id="email"
                                            formControlProps={{
                                                fullWidth: true
                                            }}
                                            inputProps={{
                                                onChange: inputEmail,
                                                type: "email",
                                                endAdornment: (
                                                    <InputAdornment position="end">
                                                        <MailIcon className={classes.inputIconsColor}>
                                                            lock_outline
                                                    </MailIcon>
                                                    </InputAdornment>
                                                ),
                                                autoComplete: "off"
                                            }}
                                        />
                                        <CustomInput
                                            labelText="Password"
                                            id="pass"
                                            formControlProps={{
                                                fullWidth: true
                                            }}
                                            inputProps={{
                                                onChange: inputPassword,
                                                type: "password",
                                                endAdornment: (
                                                    <InputAdornment position="end">
                                                        <LockIcon className={classes.inputIconsColor}>
                                                            lock_outline
                                                         </LockIcon>
                                                    </InputAdornment>
                                                ),
                                                autoComplete: "off"
                                            }}
                                        />

                                    </CardBody>
                                    {/* <FormControl component="fieldset" className={classes.chooseRoleForm}>
                                        <FormLabel component="legend" style={{ fontSize: "14px" }}>Choose your role</FormLabel>
                                        <RadioGroup aria-label="position" name="position" value={value} onChange={handleChange} row>
                                            <FormControlLabel
                                                value="a"
                                                control={<Radio style={{ color: "#ff5722" }} />}
                                                label="Member"
                                                labelPlacement="end"
                                            />
                                            <FormControlLabel
                                                value="b"
                                                control={<Radio style={{ color: "#ff5722" }} />}
                                                label="Admin"
                                                labelPlacement="end"
                                            />
                                        </RadioGroup>
                                    </FormControl> */}
                                    <CardFooter className={classes.cardFooter}>
                                        <Button variant="contained" size="lg" className={classes.registerBtn} onClick={handleRegister}>
                                            Sign Up
                                        </Button>
                                    </CardFooter>
                                    <CardFooter className={classes.loginSection}>
                                        <Typography style={{ fontSize: "12px" }}>Already have an account?</Typography>
                                        <Link to="/login" className={classes.loginLink}>
                                            Login Here
                                        </Link>
                                    </CardFooter>
                                </form>
                            </Card>
                        </GridItem>
                    </GridContainer>
                </div>
            </div>
        </div >
    );
}
