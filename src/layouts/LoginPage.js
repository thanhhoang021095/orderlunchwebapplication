import React, { useContext, useState } from "react";
import { Link } from "react-router-dom";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
// @material-ui/icons
import TrendingFlatIcon from '@material-ui/icons/TrendingFlat';
import PersonIcon from '@material-ui/icons/Person';
import LockIcon from '@material-ui/icons/Lock';
// core components
// import Header from "components/Header/Header.js";
// import HeaderLinks from "components/Header/HeaderLinks.js";
// import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import CardFooter from "components/Card/CardFooter.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import callApi from "utils/callApi/callApi"

import styles from "assets/jss/material-dashboard-react/layouts/loginPageStyle.js";

import image from "assets/img/food-bg.jpg";
import { ThemeContext } from "ThemeContext";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles(styles);

export default function LoginPage(props) {
  const classes = useStyles();
  // COMPONENT STATE
  const [userName, setUserName] = useState("");
  const [passWord, setPassWord] = useState("");
  // THEMECONTEXT
  const { setIsLoged, setIsAdmin } = useContext(ThemeContext)

  // INPUT USERNAME
  const inputUsername = (e) => {
    setUserName(e.target.value);
  }
  // INPUT PASSWORD
  const inputPassword = (e) => {
    setPassWord(e.target.value);
  }
  // HANDLE LOGIN
  const handleLogin = async () => {
    const jwt_decode = require('jwt-decode');
    try {
      const result = await callApi.login('/Account/Login', {
        username: userName,
        password: passWord,
      });
      if (result !== undefined) {
        alert("LOGIN SUCCESS");
        // PARSE TOKEN CODE
        var rawTokenCode = result.token;
        // SAVE TOKEN TO LOCALSTORAGE
        localStorage.setItem("token",rawTokenCode);
        var tokenCode = jwt_decode(rawTokenCode);
        // CHECK VALID LOGIN
        if (tokenCode.Role === "Admin") {
          setIsAdmin(true);
          setIsLoged(true);
          props.history.push("/admin/supplier");
        }
        else {
          setIsLoged(true);
          props.history.push("/dailymenu");
        };
      }
      else {
        alert("LOGIN FAILED");
      }
    }
    catch (error) {
      console.log(error)
    };

  };

  const [cardAnimaton, setCardAnimation] = React.useState("cardHidden");
  setTimeout(function () {
    setCardAnimation("");
  }, 700);

  return (
    <div>
      <div
        className={classes.pageHeader}
        style={{
          backgroundImage: "url(" + image + ")",
          backgroundSize: "cover",
          backgroundPosition: "top center"
        }}
      >
        <div className={classes.container} style={{ height: "100vh" }}>
          <GridContainer justify="center" className={classes.loginForm} style={{ padding: "50px 0px" }}>
            <GridItem lg={12} sm={12} md={12} className={classes.loginFormItem}>
              <Card className={classes[cardAnimaton]}>
                <form className={classes.form}>
                  <CardHeader className={classes.cardHeader}>
                    <h4 className={classes.loginTitle}>Login Form</h4>
                  </CardHeader>
                  <CardBody className={classes.inputSection}>
                    <CustomInput
                      labelText="Username..."
                      id="username"
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        onChange: inputUsername,
                        type: "username",
                        endAdornment: (
                          <InputAdornment position="end">
                            <PersonIcon className={classes.inputIconsColor} />
                          </InputAdornment>
                        )
                      }}
                    />
                    <CustomInput
                      labelText="Password"
                      id="password"
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        onChange: inputPassword,
                        type: "password",
                        endAdornment: (
                          <InputAdornment position="end">
                            <LockIcon className={classes.inputIconsColor}>
                              lock_outline
                            </LockIcon>
                          </InputAdornment>
                        ),
                        autoComplete: "off"
                      }}
                    />
                  </CardBody>
                  <CardFooter className={classes.cardFooter}>
                    <Button variant="contained" size="lg" className={classes.loginBtn} onClick={handleLogin}>
                      Sign In
                    </Button>
                  </CardFooter>
                  <CardFooter className={classes.registerSection}>
                    <GridContainer justify="center" className={classes.registerContainer} style={{ width: "100%" }}>
                      <GridItem xs={12} sm={12} md={12} lg={12} className={classes.registerTitleGrid} style={{ textAlign: "center" }}>
                        <Typography style={{ fontSize: "12px" }}>Have not account yet?</Typography>
                      </GridItem>
                      <GridItem xs={12} sm={12} md={12} lg={12} className={classes.registerBtnGrid}>
                        <Link to="/register">
                          <Button variant="outlined" className={classes.registerBtn} >
                            Register Now <TrendingFlatIcon style={{ marginLeft: "10px" }} />
                          </Button>
                        </Link>
                      </GridItem>
                    </GridContainer>
                  </CardFooter>
                </form>
              </Card>
            </GridItem>
          </GridContainer>
        </div>
        {/* <Footer whiteFont /> */}
      </div>
    </div>
  );
}
