import React from "react";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";

// core components
import Admin from "layouts/Admin.js";
import Login from "layouts/LoginPage.js";
import Register from "layouts/RegisterPage.js";
import DailyMenu from "layouts/DailyMenu.js"

import "assets/css/material-dashboard-react.css?v=1.8.0";

const hist = createBrowserHistory();

export default function Main() {
  return (
  <Router history={hist}>
    <Switch>
    <Route path="/login" component={Login} />
      <Route path="/register" component={Register} />
      <Route path="/admin" component={Admin} />
      {/* <Route path="/rtl" component={RTL} /> */}
      <Route path="/dailymenu" component={DailyMenu} />
      <Redirect from="/" to="/login" />
    </Switch>
  </Router>
  );
};
